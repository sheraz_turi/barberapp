package com.sheraz.barbarappdesign.ui.inbox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.adapter.InboxMessagesAdapter;
import com.sheraz.barbarappdesign.model.MessagesModelClass;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class ChatActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private String saveCurrentTime, saveCurrentDate, senderUserName, receiverUserName;
    private ImageButton btnSendMessage, btnSendFiles, btnVoiceMessage;
    private EditText et_typeMessage;
    private List<MessagesModelClass> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerViewMessagesList;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseFirestore db;
    private ProgressBar progressBar;
    private InboxMessagesAdapter inboxMessagesAdapter;

    private String checker = "", myUrl = "";
    private Uri fileUri;
    private StorageTask uploadTask;

    private ProgressDialog progressDialog;

    private MediaRecorder mediaRecorder;
    private MediaPlayer player;
    private String fileName;

    private DocumentSnapshot lastVisible;
    private boolean isScrolling;
    private boolean isLastItemReached;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initViews();

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat currentDate = new SimpleDateFormat("MM, dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
        saveCurrentTime = currentTime.format(calendar.getTime());

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        senderUserName = pref.getString("name", "default");

        Intent intent = getIntent();
        receiverUserName = intent.getStringExtra("name");

        // Record to the external cache directory for visibility
        fileName = getExternalCacheDir().getAbsolutePath();
        fileName += "/RecordedAudio" + Math.random() + "." + "mp3";
        Log.i("ok", fileName);

        buttonsClick();


    }


    private void initViews() {

        toolbar = findViewById(R.id.chat_activity_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("User Name");

        btnSendMessage = findViewById(R.id.ibtn_send);
        et_typeMessage = findViewById(R.id.tv_message);
        progressBar = findViewById(R.id.progressbar);
        btnSendFiles = findViewById(R.id.send_files_button);
        progressDialog = new ProgressDialog(this);
        btnVoiceMessage = findViewById(R.id.ibtn_voice_message);

        recyclerViewMessagesList = findViewById(R.id.private_chat_messages_list);

        firebaseFirestore = FirebaseFirestore.getInstance();
        db = FirebaseFirestore.getInstance();

    }

    private void setUpRecyclerView() {

        Query query = db.collection("Messages").orderBy("serverTimeStamp");

        FirestoreRecyclerOptions<MessagesModelClass> options = new FirestoreRecyclerOptions.Builder<MessagesModelClass>()
                .setQuery(query, MessagesModelClass.class)
                .build();

        inboxMessagesAdapter = new InboxMessagesAdapter(options);

        recyclerViewMessagesList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewMessagesList.setLayoutManager(linearLayoutManager);
        recyclerViewMessagesList.setAdapter(inboxMessagesAdapter);

        if (inboxMessagesAdapter != null) {
            recyclerViewMessagesList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recyclerViewMessagesList.smoothScrollToPosition(inboxMessagesAdapter.getItemCount());
                }
            }, 5);
        }


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                inboxMessagesAdapter.deleteMessage(viewHolder.getAdapterPosition());
                Toast.makeText(ChatActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerViewMessagesList);


        inboxMessagesAdapter.setOnItemClickListener(new InboxMessagesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {

                MessagesModelClass modelClass = documentSnapshot.toObject(MessagesModelClass.class);
                String type = modelClass.getType();
                final String fileName = modelClass.getName();
                final String message = modelClass.getMessage();
                final String id = documentSnapshot.getId();

                Toast.makeText(ChatActivity.this, "Name= " + fileName + " Type= " + type + "\n" + "id=" + id, Toast.LENGTH_SHORT).show();


                if (type.equals("image")) {

                    CharSequence options[] = new CharSequence[]
                            {

                                    "View Image",
                                    "Delete",
                                    "cancel"

                            };

                    AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                    builder.setTitle("Delete Message");

                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int pos2) {

                            if (pos2 == 0) {

                                imageDialog(Uri.parse(message));

                            } else if (pos2 == 1) {

                                deleteImageFile(id, fileName);

                            }

                        }
                    });

                    builder.show();


                } else if (type.equals("pdf") || type.equals("doc")) {

                    CharSequence options[] = new CharSequence[]
                            {

                                    "View File",
                                    "Delete",
                                    "Cancel"

                            };

                    AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                    builder.setTitle("Delete Message");

                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int pos) {

                            if (pos == 0) {

                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message));
                                startActivity(intent);

                            } else if (pos == 1) {

                                deleteDocumentFile(id, fileName);

                            }

                        }
                    });

                    builder.show();


                }


            }
        });


    }


    private void buttonsClick() {

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });


        btnSendFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CharSequence options[] = new CharSequence[]
                        {
                                "Images",
                                "PDF Files",
                                "MS Word Files"
                        };

                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle("Select The File");

                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {

                        if (i == 0) {
                            checker = "image";

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            startActivityForResult(Intent.createChooser(intent, "Select Image"), 438);

                        }

                        if (i == 1) {
                            checker = "pdf";

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            startActivityForResult(Intent.createChooser(intent, "Select PDF File"), 438);

                        }

                        if (i == 2) {
                            checker = "doc";

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/msword");
                            startActivityForResult(Intent.createChooser(intent, "Select Word File"), 438);
                        }

                    }
                });

                builder.show();

            }
        });

        btnVoiceMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                    startRecording();
                    Toast.makeText(ChatActivity.this, "Recording is start", Toast.LENGTH_SHORT).show();

                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                    stopRecording();
                    Toast.makeText(ChatActivity.this, "Recording is stopped", Toast.LENGTH_SHORT).show();
                    uploadAudioFile();

                }

                return false;
            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();

        setUpRecyclerView();
        inboxMessagesAdapter.startListening();
        progressBar.setVisibility(View.GONE);

    }

    @Override
    protected void onStop() {
        super.onStop();

        inboxMessagesAdapter.startListening();
    }

    //sending text messages
    private void sendMessage() {

        final String messageText = et_typeMessage.getText().toString();

        if (TextUtils.isEmpty(messageText)) {
            Toast.makeText(ChatActivity.this, "Please write your message...", Toast.LENGTH_SHORT).show();
        } else {

            CollectionReference messagesCollection = firebaseFirestore.collection("Messages");

            Map messageTextBody = new HashMap();
            messageTextBody.put("message", messageText);
            messageTextBody.put("type", "text");
            messageTextBody.put("from", senderUserName);
            messageTextBody.put("to", receiverUserName);
            messageTextBody.put("time", saveCurrentTime);
            messageTextBody.put("date", saveCurrentDate);
            messageTextBody.put("serverTimeStamp", FieldValue.serverTimestamp());

            messagesCollection.add(messageTextBody).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {

                    et_typeMessage.setText("");

                    recyclerViewMessagesList.smoothScrollToPosition(recyclerViewMessagesList.getAdapter().getItemCount());

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(ChatActivity.this, "Data added Failed", Toast.LENGTH_SHORT).show();
                    et_typeMessage.setText("");

                }
            });


        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 438 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            progressDialog.setTitle("Sending File");
            progressDialog.setMessage("Please Wait, file is sending...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            fileUri = data.getData();

            if (!checker.equals("image")) {

                sendDocumentFile();

            } else if (checker.equals("image")) {

                sendImageFile();

            } else {
                progressDialog.dismiss();
                Toast.makeText(this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //sending pdf or word file
    private void sendDocumentFile() {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Document Files");

        final CollectionReference messagesCollection = firebaseFirestore.collection("Messages");

        final String documentName = fileUri.getLastPathSegment();

        final StorageReference filePath = storageReference.child(documentName + "." + checker);

        uploadTask = filePath.putFile(fileUri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {

                if (!task.isSuccessful()) {

                    throw task.getException();

                }

                return filePath.getDownloadUrl();

            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {

                if (task.isSuccessful()) {

                    Uri documentUri = task.getResult();
                    myUrl = documentUri.toString();

                    Map messageImageBody = new HashMap();
                    messageImageBody.put("message", myUrl);
                    messageImageBody.put("name", fileUri.getLastPathSegment() + "." + checker);
                    messageImageBody.put("type", checker);
                    messageImageBody.put("from", senderUserName);
                    messageImageBody.put("to", receiverUserName);
                    messageImageBody.put("time", saveCurrentTime);
                    messageImageBody.put("date", saveCurrentDate);
                    messageImageBody.put("serverTimeStamp", FieldValue.serverTimestamp());

                    messagesCollection.add(messageImageBody).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {

                            progressDialog.dismiss();
                            Toast.makeText(ChatActivity.this, "File Sent", Toast.LENGTH_SHORT).show();
                            recyclerViewMessagesList.smoothScrollToPosition(recyclerViewMessagesList.getAdapter().getItemCount());

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(ChatActivity.this, "File sending Failed", Toast.LENGTH_SHORT).show();

                        }
                    });


                }

            }
        });

    }


    //sending image file
    private void sendImageFile() {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Image Files");

        final CollectionReference messagesCollection = firebaseFirestore.collection("Messages");

        final String imageName = fileUri.getLastPathSegment();

        final StorageReference filePath = storageReference.child(imageName + "." + "jpg");

        uploadTask = filePath.putFile(fileUri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {

                if (!task.isSuccessful()) {

                    throw task.getException();

                }

                return filePath.getDownloadUrl();

            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {

                if (task.isSuccessful()) {

                    Uri imageUri = task.getResult();
                    myUrl = imageUri.toString();

                    Map messageImageBody = new HashMap();
                    messageImageBody.put("message", myUrl);
                    messageImageBody.put("name", fileUri.getLastPathSegment() + "." + "jpg");
                    messageImageBody.put("type", checker);
                    messageImageBody.put("from", senderUserName);
                    messageImageBody.put("to", receiverUserName);
                    messageImageBody.put("messageID", imageName);
                    messageImageBody.put("time", saveCurrentTime);
                    messageImageBody.put("date", saveCurrentDate);
                    messageImageBody.put("serverTimeStamp", FieldValue.serverTimestamp());

                    messagesCollection.add(messageImageBody).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {

                            progressDialog.dismiss();
                            Toast.makeText(ChatActivity.this, "File Sent", Toast.LENGTH_SHORT).show();
                            recyclerViewMessagesList.smoothScrollToPosition(recyclerViewMessagesList.getAdapter().getItemCount());

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(ChatActivity.this, "File sending Failed", Toast.LENGTH_SHORT).show();

                        }
                    });


                }

            }
        });


    }


    private void startRecording() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setOutputFile(fileName);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            Log.e("Record", "prepare() failed");
        }

        mediaRecorder.start();
    }

    private void stopRecording() {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
    }

    public void startPlaying() {
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            Log.i("ok", fileName);
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e("play", "prepare() failed");
        }
    }

    public void stopPlaying() {
        player.release();
        player = null;
    }

    private void uploadAudioFile() {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Audio Files");

        final CollectionReference audioCollection = firebaseFirestore.collection("Messages");

        final Uri uri = Uri.fromFile(new File(fileName));
        final String audioName = uri.getLastPathSegment();

        final StorageReference filePath = storageReference.child(audioName);


        uploadTask = filePath.putFile(uri);

        uploadTask.continueWithTask(new Continuation() {
            @Override
            public Object then(@NonNull Task task) throws Exception {

                if (!task.isSuccessful()) {

                    throw task.getException();

                }

                return filePath.getDownloadUrl();

            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {

                if (task.isSuccessful()) {

                    Uri downloadUri = task.getResult();
                    myUrl = downloadUri.toString();

                    Map messageImageBody = new HashMap();
                    messageImageBody.put("message", myUrl);
                    messageImageBody.put("name", uri.getLastPathSegment());
                    messageImageBody.put("type", "audio");
                    messageImageBody.put("from", senderUserName);
                    messageImageBody.put("to", receiverUserName);
                    messageImageBody.put("messageID", "");
                    messageImageBody.put("time", saveCurrentTime);
                    messageImageBody.put("date", saveCurrentDate);
                    messageImageBody.put("serverTimeStamp", FieldValue.serverTimestamp());

                    audioCollection.add(messageImageBody).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {

                            progressDialog.dismiss();
                            Toast.makeText(ChatActivity.this, "File Sent", Toast.LENGTH_SHORT).show();
                            recyclerViewMessagesList.smoothScrollToPosition(recyclerViewMessagesList.getAdapter().getItemCount());

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(ChatActivity.this, "File sending Failed", Toast.LENGTH_SHORT).show();

                        }
                    });


                }

            }
        });

    }


    //Deleting an image
    private void deleteImageFile(final String id, final String name) {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Image Files");

        storageReference.child(name).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("Messages").document(id)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ChatActivity.this, "Successfully deleted!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ChatActivity.this, "Error deleting document", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });

    }


    //Deleting document
    private void deleteDocumentFile(final String id, final String name) {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Document Files");

        storageReference.child(name).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("Messages").document(id)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ChatActivity.this, "Successfully deleted!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ChatActivity.this, "Error deleting document", Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });

    }


    //Deleting Audio
    private void deleteAudioFile(final String id, final String name) {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Audio Files");

        storageReference.child(name).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("Messages").document(id)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ChatActivity.this, "Successfully deleted!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ChatActivity.this, "Error deleting document", Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });

    }


    private void imageDialog(final Uri imgUri) {

        Rect displayRectangle = new Rect();
        Window window = ChatActivity.this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this, R.style.CustomAlertDialog);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ChatActivity.this).inflate(R.layout.custom_image_dialog, viewGroup, false);
        dialogView.setMinimumWidth((int) (displayRectangle.width() * 1f));
        dialogView.setMinimumHeight((int) (displayRectangle.height() * 1f));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        PhotoView ivFullScreen = dialogView.findViewById(R.id.iv_full_screen);

        if (imgUri != null) {

            Glide.with(ChatActivity.this).load(imgUri).into(ivFullScreen);

        }

        alertDialog.show();

    }


}
