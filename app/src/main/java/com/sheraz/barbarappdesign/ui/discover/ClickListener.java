package com.sheraz.barbarappdesign.ui.discover;

public interface ClickListener {

    void onPositionClicked(int position);

    void onLongClicked(int position);

}
