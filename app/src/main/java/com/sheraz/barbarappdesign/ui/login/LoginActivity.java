package com.sheraz.barbarappdesign.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.databinding.ActivityLoginBinding;
import com.sheraz.barbarappdesign.model.UserLoginModel;
import com.sheraz.barbarappdesign.ui.MainActivity;
import com.sheraz.barbarappdesign.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity implements LoginCallback {

    EditText et_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setViewModel(ViewModelProviders.of(this, new LoginViewModelFactory(this)).get(LoginViewModel.class));

        et_name = findViewById(R.id.et_login_name);
}

    @Override
    public void onSuccess(String message) {

        String userName = et_name.getText().toString();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("name",userName);
        editor.commit();

        Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
    }
}
