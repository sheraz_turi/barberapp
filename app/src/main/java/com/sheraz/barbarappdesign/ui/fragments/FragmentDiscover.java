package com.sheraz.barbarappdesign.ui.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.adapter.AdapterDiscoverFragment;
import com.sheraz.barbarappdesign.model.DiscoverModelClass;

import java.util.ArrayList;
import java.util.List;

public class FragmentDiscover extends Fragment {

    RecyclerView recyclerView;
    List<DiscoverModelClass> discoverModelClassList = new ArrayList<>();

    private ArrayList<DiscoverModelClass> mExampleList = new ArrayList<>();

    AdapterDiscoverFragment adapterDiscoverFragment;

    EditText et_searchBarber;
    int textlength = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_discover, container, false);

        recyclerView = view.findViewById(R.id.rv_discover_fragment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        et_searchBarber = view.findViewById(R.id.et_search_barber);

        recyclerListFilter();
        getAllData();


        return view;
    }

    public void getAllData() {

        DiscoverModelClass discoverModelClass = new DiscoverModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "Daniel Willaim", "Barber at redbox Barber", "$5", "2.5km");
        discoverModelClassList.add(discoverModelClass);

        DiscoverModelClass discoverModelClass1 = new DiscoverModelClass("https://api.androidhive.info/images/nature/david.jpg", "Richardson", "Barber at redbox Barber", "$53", "2.35km");
        discoverModelClassList.add(discoverModelClass1);

        DiscoverModelClass discoverModelClass2 = new DiscoverModelClass("https://pngimage.net/wp-content/uploads/2018/05/barber-cartoon-png-2.png", "Ross Taylor", "Barber at redbox Barber", "$15", "1.9km");
        discoverModelClassList.add(discoverModelClass2);

        DiscoverModelClass discoverModelClass3 = new DiscoverModelClass("https://pngimage.net/wp-content/uploads/2018/05/barber-cartoon-png-2.png", "Kevin Peterson", "Barber at redbox Barber", "$5", "2.5km");
        discoverModelClassList.add(discoverModelClass3);

        DiscoverModelClass discoverModelClass4 = new DiscoverModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "Johnson dam", "Barber at redbox Barber", "$5", "2.5km");
        discoverModelClassList.add(discoverModelClass4);

        DiscoverModelClass discoverModelClass5 = new DiscoverModelClass("https://pngimage.net/wp-content/uploads/2018/05/barber-cartoon-png-2.png", "Daniel Willaim", "Barber at redbox Barber", "$7", "8.1km");
        discoverModelClassList.add(discoverModelClass5);

        DiscoverModelClass discoverModelClass6 = new DiscoverModelClass("https://api.androidhive.info/images/nature/david.jpg", "Daniel Willaim", "Barber at redbox Barber", "$5", "2.5km");
        discoverModelClassList.add(discoverModelClass6);

        DiscoverModelClass discoverModelClass7 = new DiscoverModelClass("https://pngimage.net/wp-content/uploads/2018/05/barber-cartoon-png-2.png", "Daniel Willaim", "Barber at redbox Barber", "$5", "2.5km");
        discoverModelClassList.add(discoverModelClass7);


        adapterDiscoverFragment = new AdapterDiscoverFragment(discoverModelClassList, getContext());
        recyclerView.setAdapter(adapterDiscoverFragment);
        recyclerView.setNestedScrollingEnabled(false);

    }

    public void recyclerListFilter() {

        et_searchBarber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        et_searchBarber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                textlength = et_searchBarber.getText().length();
                mExampleList.clear();
                for (int i = 0; i < discoverModelClassList.size(); i++) {
                    if (textlength <= discoverModelClassList.get(i).getName().length()) {
                        Log.d("ertyyy", discoverModelClassList.get(i).getName().toLowerCase().trim());
                        if (discoverModelClassList.get(i).getName().toLowerCase().trim().contains(
                                et_searchBarber.getText().toString().toLowerCase().trim())) {
                            mExampleList.add(discoverModelClassList.get(i));
                        }
                    }
                }
                adapterDiscoverFragment = new AdapterDiscoverFragment(mExampleList, getContext());
                recyclerView.setAdapter(adapterDiscoverFragment);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
