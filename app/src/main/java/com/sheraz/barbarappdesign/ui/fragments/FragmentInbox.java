package com.sheraz.barbarappdesign.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.adapter.AdapterInboxFragment;
import com.sheraz.barbarappdesign.model.InboxModelClass;
import com.sheraz.barbarappdesign.model.MessagesModelClass;

import java.util.ArrayList;
import java.util.List;

public class FragmentInbox extends Fragment {

    RecyclerView recyclerView;
    Toolbar toolbar;

    List<MessagesModelClass> messagesModelClassList = new ArrayList<>();
    AdapterInboxFragment adapterInboxFragment;

    List<InboxModelClass> modelClassList = new ArrayList<>();

    FirebaseFirestore firebaseFirestore;
    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inbox, container, false);

        recyclerView = view.findViewById(R.id.rv_fragment_inbox);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        firebaseFirestore = FirebaseFirestore.getInstance();
        db = FirebaseFirestore.getInstance();

        final InboxModelClass inboxModelClass = new InboxModelClass("http://via.placeholder.com/300.png", "Kallis", "how are you", "2 min ago", "2");
        modelClassList.add(inboxModelClass);

        InboxModelClass inboxModelClass2 = new InboxModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "James anderson", "where are you?", "5 min ago", "1");
        modelClassList.add(inboxModelClass2);

        InboxModelClass inboxModelClass3 = new InboxModelClass("http://via.placeholder.com/300.png", "Willson jack", "hi", "online", "");
        modelClassList.add(inboxModelClass3);

        InboxModelClass inboxModelClass4 = new InboxModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "Johnson", "nice to meet you", "1 min ago", "1");
        modelClassList.add(inboxModelClass4);

        InboxModelClass inboxModelClass5 = new InboxModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "Ross Taylor", "its ok", "7 min ago", "2");
        modelClassList.add(inboxModelClass5);

        InboxModelClass inboxModelClass6 = new InboxModelClass("https://img.pngio.com/beard-goatee-clip-art-barber-24001450-transprent-png-free-man-with-beard-png-260_260.jpg", "Williamson", "Today I am not comming", "2 min ago", "2");
        modelClassList.add(inboxModelClass6);

        InboxModelClass inboxModelClass7 = new InboxModelClass("https://pngimage.net/wp-content/uploads/2018/05/barber-cartoon-png-2.png", "Brad Haddin", "See you soon", "2 min ago", "2");
        modelClassList.add(inboxModelClass7);

        InboxModelClass inboxModelClass8 = new InboxModelClass("http://via.placeholder.com/300.png", "sherazTuri", "You are always welcome", "1 min ago", "2");
        modelClassList.add(inboxModelClass8);


        adapterInboxFragment = new AdapterInboxFragment(modelClassList, getContext());
        recyclerView.setAdapter(adapterInboxFragment);
        adapterInboxFragment.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);


        return view;
    }


}
