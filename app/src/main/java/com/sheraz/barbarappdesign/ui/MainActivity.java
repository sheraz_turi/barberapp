package com.sheraz.barbarappdesign.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.ui.fragments.FragmentBookmark;
import com.sheraz.barbarappdesign.ui.fragments.FragmentDiscover;
import com.sheraz.barbarappdesign.ui.fragments.FragmentInbox;
import com.sheraz.barbarappdesign.ui.fragments.FragmentNearby;
import com.sheraz.barbarappdesign.ui.fragments.FragmentProfile;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initViews();


    }

    private void initViews() {
        bottomNavigationView = findViewById(R.id.bnv);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, new FragmentDiscover()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            Fragment selectedFragment = null;

            switch (menuItem.getItemId()) {

                case R.id.mi_discover:
                    selectedFragment = new FragmentDiscover();
                    break;

                case R.id.mi_nearby:
                    selectedFragment = new FragmentNearby();
                    break;

                case R.id.mi_inbox:
                    selectedFragment = new FragmentInbox();
                    break;

                case R.id.mi_bookmarks:
                    selectedFragment = new FragmentBookmark();
                    break;

                case R.id.mi_profile:
                    selectedFragment = new FragmentProfile();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, selectedFragment).commit();

            return true;
        }
    };
}
