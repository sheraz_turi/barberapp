package com.sheraz.barbarappdesign.ui.login;

public interface LoginCallback {

    public  void onSuccess(String message);

    public  void onFailure(String message);
}
