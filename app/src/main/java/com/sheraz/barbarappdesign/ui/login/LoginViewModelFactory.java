package com.sheraz.barbarappdesign.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.sheraz.barbarappdesign.model.UserLoginModel;
import com.sheraz.barbarappdesign.viewmodel.LoginViewModel;

public class LoginViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private LoginCallback loginCallback;

    public LoginViewModelFactory(LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new  LoginViewModel(loginCallback);
    }
}
