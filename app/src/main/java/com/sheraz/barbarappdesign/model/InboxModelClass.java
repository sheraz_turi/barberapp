package com.sheraz.barbarappdesign.model;

public class InboxModelClass {

    String profileImage;
    String name, lastMessage, lastSeen, numberOfMessages;
    private String id;


    public InboxModelClass(String profileImage, String name, String lastMessage, String lastSeen, String numberOfMessages) {
        this.profileImage = profileImage;
        this.name = name;
        this.lastMessage = lastMessage;
        this.lastSeen = lastSeen;
        this.numberOfMessages = numberOfMessages;
    }

    public InboxModelClass(String name, String lastMessage, String lastSeen, String numberOfMessages) {
        this.name = name;
        this.lastMessage = lastMessage;
        this.lastSeen = lastSeen;
        this.numberOfMessages = numberOfMessages;
    }

    public InboxModelClass() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String  profileImage) {
        this.profileImage = profileImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getNumberOfMessages() {
        return numberOfMessages;
    }

    public void setNumberOfMessages(String numberOfMessages) {
        this.numberOfMessages = numberOfMessages;
    }
}
