package com.sheraz.barbarappdesign.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;

public class MessagesModelClass implements Serializable {

    @Exclude
    private String id;

    private String from, message, type, to, messageID, time, date, name;

    private String staticMessage, staticTime, staticDate, senderName, receiverName;

    @ServerTimestamp
    private Date dateSentTo;

    public MessagesModelClass() {
    }

    public MessagesModelClass(String staticMessage, String staticTime, String staticDate, String senderName, String receiverName) {
        this.staticMessage = staticMessage;
        this.staticTime = staticTime;
        this.staticDate = staticDate;
        this.senderName = senderName;
        this.receiverName = receiverName;
    }

    public MessagesModelClass(String from, String message, String type, String to, String messageID, String time, String date, String name) {
        this.from = from;
        this.message = message;
        this.type = type;
        this.to = to;
        this.messageID = messageID;
        this.time = time;
        this.date = date;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStaticMessage() {
        return staticMessage;
    }

    public void setStaticMessage(String staticMessage) {
        this.staticMessage = staticMessage;
    }

    public String getStaticTime() {
        return staticTime;
    }

    public void setStaticTime(String staticTime) {
        this.staticTime = staticTime;
    }

    public String getStaticDate() {
        return staticDate;
    }

    public void setStaticDate(String staticDate) {
        this.staticDate = staticDate;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
