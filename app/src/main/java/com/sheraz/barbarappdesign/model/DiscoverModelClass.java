package com.sheraz.barbarappdesign.model;

import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DiscoverModelClass extends BaseObservable {

    @Nullable
    String avatarImg, cardImg;

    @Nullable
    String name, description, rating, startsFrom, distance;

    public DiscoverModelClass() {
    }

    public DiscoverModelClass(@Nullable String avatarImg, @Nullable String name, @Nullable String description, @Nullable String startsFrom, @Nullable String distance) {
        this.avatarImg = avatarImg;
        this.name = name;
        this.description = description;
        this.startsFrom = startsFrom;
        this.distance = distance;
    }

    @Nullable
    public String getAvatarImg() {
        return avatarImg;
    }

    public void setAvatarImg(@Nullable String avatarImg) {
        this.avatarImg = avatarImg;
    }

    @Nullable
    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(@Nullable String cardImg) {
        this.cardImg = cardImg;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public String getRating() {
        return rating;
    }

    public void setRating(@Nullable String rating) {
        this.rating = rating;
    }

    @Nullable
    public String getStartsFrom() {
        return startsFrom;
    }

    public void setStartsFrom(@Nullable String startsFrom) {
        this.startsFrom = startsFrom;
    }

    @Nullable
    public String getDistance() {
        return distance;
    }

    public void setDistance(@Nullable String distance) {
        this.distance = distance;
    }


}
