package com.sheraz.barbarappdesign.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import org.w3c.dom.Text;

public class UserLoginModel {

    @Nullable
    String name, password;

    public UserLoginModel() {
    }

    public UserLoginModel(@Nullable String name, @Nullable String password) {
        this.name = name;
        this.password = password;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    public boolean isValid(){
        return  !TextUtils.isEmpty(name) && !TextUtils.isEmpty(password);
    }
}
