package com.sheraz.barbarappdesign.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.model.InboxModelClass;
import com.sheraz.barbarappdesign.model.MessagesModelClass;
import com.sheraz.barbarappdesign.ui.inbox.ChatActivity;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterInboxFragment extends RecyclerView.Adapter<AdapterInboxFragment.MyViewHolder> {

    List<InboxModelClass> modelClassList;
    List<MessagesModelClass> messagesModelClassList = new ArrayList<>();
    Context mContext;

    public AdapterInboxFragment(List<InboxModelClass> modelClassList, Context mContext) {
        this.modelClassList = modelClassList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_inbox_rv_layout, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        InboxModelClass inboxModelClass = modelClassList.get(position);
        holder.tv_name.setText(inboxModelClass.getName());
        holder.tv_lastMessage.setText(inboxModelClass.getLastMessage());
        holder.tv_lastSeen.setText(inboxModelClass.getLastSeen());
        holder.tv_noOfMessages.setText(inboxModelClass.getNumberOfMessages());

        Glide.with(mContext).load(inboxModelClass.getProfileImage()).into(holder.circleImageView);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getAdapterPosition();
                if (String.valueOf(pos) != null) {
                    String userName = holder.tv_name.getText().toString();

                    SharedPreferences pref = mContext.getSharedPreferences("MyPrefName", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("userName", userName);
                    editor.commit();


                    Intent intent = new Intent(mContext, ChatActivity.class);
                    intent.putExtra("name", userName);
                    mContext.startActivity(intent);
                    Toast.makeText(mContext, userName, Toast.LENGTH_SHORT).show();

                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelClassList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;
        TextView tv_name, tv_lastMessage, tv_lastSeen, tv_noOfMessages;
        RelativeLayout relativeLayout;
        String messageSenderName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = itemView.findViewById(R.id.inbox_profile_image);
            tv_name = itemView.findViewById(R.id.inbox_barbar_name);
            tv_lastMessage = itemView.findViewById(R.id.inbox_barbar_lastMessage);
            tv_lastSeen = itemView.findViewById(R.id.inbox_tv_lastseen);
            tv_noOfMessages = itemView.findViewById(R.id.inbox_tv_message_quantity);

            relativeLayout = itemView.findViewById(R.id.rl_inbox);

            SharedPreferences pref = itemView.getContext().getSharedPreferences("MyPref", 0);
            messageSenderName = pref.getString("name", "default");

        }
    }
}
