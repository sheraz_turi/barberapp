package com.sheraz.barbarappdesign.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.model.DiscoverModelClass;
import com.sheraz.barbarappdesign.model.MessagesModelClass;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterDiscoverFragment extends RecyclerView.Adapter<AdapterDiscoverFragment.MyViewHolder> {

    List<DiscoverModelClass> modelClassList;
    List<MessagesModelClass> messagesModelClassList = new ArrayList<>();
    Context mContext;

    public AdapterDiscoverFragment(List<DiscoverModelClass> modelClassList, Context mContext) {
        this.modelClassList = modelClassList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_discover_rv_layout, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        DiscoverModelClass discoverModelClass = modelClassList.get(position);
        holder.tv_name.setText(discoverModelClass.getName());
        holder.tv_desc.setText(discoverModelClass.getDescription());
        holder.tv_startFrom.setText(discoverModelClass.getStartsFrom());
        holder.tv_distance.setText(discoverModelClass.getDistance());

        Glide.with(mContext).load(discoverModelClass.getAvatarImg()).into(holder.circleImageView);

        holder.btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = holder.tv_name.getText().toString();
                Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();
            }
        });

//        InboxModelClass inboxModelClass = modelClassList.get(position);
//        holder.tv_name.setText(inboxModelClass.getName());
//        holder.tv_lastMessage.setText(inboxModelClass.getLastMessage());
//        holder.tv_lastSeen.setText(inboxModelClass.getLastSeen());
//        holder.tv_noOfMessages.setText(inboxModelClass.getNumberOfMessages());

//        Glide.with(mContext).load(inboxModelClass.getProfileImage()).placeholder(R.drawable.barbar2).error(R.mipmap.ic_launcher).load(inboxModelClass.getProfileImage());


//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int pos = holder.getAdapterPosition();
//                if (String.valueOf(pos) != null){
//                    String userName = holder.tv_name.getText().toString();
//
//                    SharedPreferences pref = mContext.getSharedPreferences("MyPrefName", 0);
//                    SharedPreferences.Editor editor = pref.edit();
//                    editor.putString("userName",userName);
//                    editor.commit();
//
//
//                    Intent intent = new Intent(mContext, ChatActivity.class);
//                    intent.putExtra("name",userName);
//                    mContext.startActivity(intent);
//                    Toast.makeText(mContext, userName, Toast.LENGTH_SHORT).show();
//
//                }
//                notifyDataSetChanged();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return modelClassList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;
        TextView tv_name, tv_desc, tv_startFrom, tv_distance;
        RelativeLayout relativeLayout;
        ImageView imageView1, imageView2, imageView3;
        String messageSenderName;
        Button btnOpen;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = itemView.findViewById(R.id.discover_rv_profile_image);
            tv_name = itemView.findViewById(R.id.tv_barbar_name);
            tv_desc = itemView.findViewById(R.id.tv_barber_desc);
            tv_startFrom = itemView.findViewById(R.id.txt_barbar_starts_dollars);
            tv_distance = itemView.findViewById(R.id.txt_barbar_distance_km);
            imageView1 = itemView.findViewById(R.id.iv_discover1);
            imageView2 = itemView.findViewById(R.id.iv_discover2);
            imageView3 = itemView.findViewById(R.id.iv_discover3);
            btnOpen = itemView.findViewById(R.id.btn_rv_open);

            //relativeLayout = itemView.findViewById(R.id.rl_inbox);

            SharedPreferences pref = itemView.getContext().getSharedPreferences("MyPref", 0);
            messageSenderName = pref.getString("name", "default");

        }
    }
}
