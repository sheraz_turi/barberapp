package com.sheraz.barbarappdesign.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sheraz.barbarappdesign.R;
import com.sheraz.barbarappdesign.model.MessagesModelClass;
import com.sheraz.barbarappdesign.ui.inbox.ChatActivity;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class InboxMessagesAdapter extends FirestoreRecyclerAdapter<MessagesModelClass, InboxMessagesAdapter.MessagesHolder> {

    Context context;
    OnItemClickListener listener;
    MediaPlayer player = new MediaPlayer();

    public InboxMessagesAdapter(@NonNull FirestoreRecyclerOptions<MessagesModelClass> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull final MessagesHolder holder, int position, @NonNull final MessagesModelClass model) {

        String messageType = model.getType();
        String fromUserName = model.getFrom();
        String toUserName = model.getTo();
        final String message = model.getMessage();
        final String fileName = model.getName();

        final String id = model.getId();

        holder.receiverMessageText.setVisibility(View.GONE);
        holder.receiverProfileImage.setVisibility(View.GONE);
        holder.senderMessageText.setVisibility(View.GONE);
        holder.messageSenderPicture.setVisibility(View.GONE);
        holder.messageReceiverPicture.setVisibility(View.GONE);
        holder.btnPlaySenderAudio.setVisibility(View.GONE);
        holder.btnPlayReceiverAudio.setVisibility(View.GONE);
        holder.ivSenderPdfFile.setVisibility(View.GONE);
        holder.ivReceiverPdfFile.setVisibility(View.GONE);


////////////////////////////////////////////////////////////////////////////////////////////////////
        if (messageType.equals("text")) {

            if (fromUserName.equals(holder.messageSenderName)) {

                if (toUserName.equals(holder.messageRecieverName)) {

                    holder.senderMessageText.setVisibility(View.VISIBLE);

                    holder.senderMessageText.setBackgroundResource(R.drawable.sender_messages_layout);
                    holder.senderMessageText.setText(model.getMessage() + "\n \n" + model.getTime() + "-" + model.getDate());

                }

            } else if (toUserName.equals(holder.messageSenderName) && fromUserName.equals(holder.messageRecieverName)) {

                holder.receiverMessageText.setVisibility(View.VISIBLE);
                holder.receiverProfileImage.setVisibility(View.VISIBLE);

                holder.receiverMessageText.setBackgroundResource(R.drawable.receiver_messages_layout);
                holder.receiverMessageText.setText(model.getMessage() + "\n \n" + model.getTime() + "-" + model.getDate());

            }
        } else if (messageType.equals("image")) {

            if (fromUserName.equals(holder.messageSenderName)) {

                if (toUserName.equals(holder.messageRecieverName)) {

                    holder.messageSenderPicture.setVisibility(View.VISIBLE);

                    Picasso.get().load(model.getMessage()).into(holder.messageSenderPicture);

                }

            } else if (toUserName.equals(holder.messageSenderName) && fromUserName.equals(holder.messageRecieverName)) {

                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.messageReceiverPicture.setVisibility(View.VISIBLE);

                Picasso.get().load(model.getMessage()).into(holder.messageReceiverPicture);

            }

        } else if (messageType.equals("pdf")) {

            if (fromUserName.equals(holder.messageSenderName)) {

                if (toUserName.equals(holder.messageRecieverName)) {

                    holder.ivSenderPdfFile.setVisibility(View.VISIBLE);

                }

            } else if (toUserName.equals(holder.messageSenderName) && fromUserName.equals(holder.messageRecieverName)) {

                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.ivReceiverPdfFile.setVisibility(View.VISIBLE);

            }
        } else if (messageType.equals("doc")) {

            if (fromUserName.equals(holder.messageSenderName)) {

                if (toUserName.equals(holder.messageRecieverName)) {

                    holder.ivSenderPdfFile.setVisibility(View.VISIBLE);
                    holder.ivSenderPdfFile.setImageResource(R.drawable.wordicon);

                }

            } else if (toUserName.equals(holder.messageSenderName) && fromUserName.equals(holder.messageRecieverName)) {

                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.ivReceiverPdfFile.setVisibility(View.VISIBLE);
                holder.ivReceiverPdfFile.setImageResource(R.drawable.wordicon);

            }
        } else if (messageType.equals("audio")) {

            if (fromUserName.equals(holder.messageSenderName)) {

                if (toUserName.equals(holder.messageRecieverName)) {

                    holder.btnPlaySenderAudio.setVisibility(View.VISIBLE);

                    holder.btnPlaySenderAudio.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (player.isPlaying()) {
                                player.stop();
                                player.release();

                                player = new MediaPlayer();

                                holder.btnPlaySenderAudio.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                            } else {
                                try {
                                    player.setDataSource(message);
                                    player.prepare();
                                    player.start();

                                    holder.btnPlaySenderAudio.setImageResource(R.drawable.ic_pause_black_24dp);

                                    Toast.makeText(context, "Playing", Toast.LENGTH_SHORT).show();

                                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mp) {

                                            player.stop();
                                            player.release();

                                            player = new MediaPlayer();

                                            holder.btnPlaySenderAudio.setImageResource(R.drawable.ic_play_arrow_black_24dp);

                                            Toast.makeText(context, "The End", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                } catch (IOException e) {
                                    Log.e("play", "prepare() failed");
                                }
                            }

                        }
                    });


                }

            } else if (toUserName.equals(holder.messageSenderName) && fromUserName.equals(holder.messageRecieverName)) {

                holder.btnPlayReceiverAudio.setVisibility(View.VISIBLE);
                holder.receiverProfileImage.setVisibility(View.VISIBLE);

                holder.btnPlayReceiverAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (player.isPlaying()) {
                            player.stop();
                            player.release();

                            player = new MediaPlayer();

                            holder.btnPlayReceiverAudio.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        } else {
                            try {
                                player.setDataSource(message);
                                player.prepare();
                                player.start();

                                holder.btnPlayReceiverAudio.setImageResource(R.drawable.ic_pause_black_24dp);

                                Toast.makeText(context, "Playing", Toast.LENGTH_SHORT).show();

                                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {

                                        player.stop();
                                        player.release();

                                        player = new MediaPlayer();

                                        holder.btnPlayReceiverAudio.setImageResource(R.drawable.ic_play_arrow_black_24dp);

                                        Toast.makeText(context, "The End", Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } catch (IOException e) {
                                Log.e("play", "prepare() failed");
                            }
                        }

                    }
                });

            }

        }


    }


    @NonNull
    @Override
    public MessagesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_messages_layout, parent, false);


        context = parent.getContext();

        return new MessagesHolder(view);
    }

    public class MessagesHolder extends RecyclerView.ViewHolder {

        TextView senderMessageText, receiverMessageText;
        CircleImageView receiverProfileImage;
        ImageView ivSenderPdfFile, ivReceiverPdfFile;
        ImageView messageSenderPicture, messageReceiverPicture;
        ImageButton btnPlaySenderAudio, btnPlayReceiverAudio;

        String messageSenderName, messageRecieverName;

        public MessagesHolder(@NonNull View itemView) {
            super(itemView);

            senderMessageText = itemView.findViewById(R.id.sender_message_text);
            receiverMessageText = itemView.findViewById(R.id.receiver_message_text);
            receiverProfileImage = itemView.findViewById(R.id.message_profile_image);
            messageSenderPicture = itemView.findViewById(R.id.message_sender_image_view);
            messageReceiverPicture = itemView.findViewById(R.id.message_receiver_image_view);
            btnPlaySenderAudio = itemView.findViewById(R.id.btn_sender_play_audio);
            btnPlayReceiverAudio = itemView.findViewById(R.id.btn_receiver_play_audio);
            ivSenderPdfFile = itemView.findViewById(R.id.iv_sender_pdf_file);
            ivReceiverPdfFile = itemView.findViewById(R.id.iv_receiver_pdf_file);

            SharedPreferences pref = itemView.getContext().getSharedPreferences("MyPref", 0);
            messageSenderName = pref.getString("name", "default");

            SharedPreferences pref2 = itemView.getContext().getSharedPreferences("MyPrefName", 0);
            messageRecieverName = pref2.getString("userName", "default");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });


        }
    }

    public interface OnItemClickListener {

        void onItemClick(DocumentSnapshot documentSnapshot, int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public void deleteMessage(int position) {

        getSnapshots().getSnapshot(position).getReference().delete();

    }

    //Deleting Audio
    private void deleteAudioFile(final String id, final String name) {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Audio Files");

        storageReference.child(name).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("Messages").document(id)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(context, "Successfully deleted!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(context, "Error deleting document", Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });
    }
}
