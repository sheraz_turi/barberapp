package com.sheraz.barbarappdesign.viewmodel;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.lifecycle.ViewModel;

import com.sheraz.barbarappdesign.model.UserLoginModel;
import com.sheraz.barbarappdesign.ui.login.LoginCallback;

public class LoginViewModel extends ViewModel {

    private UserLoginModel loginModel;
    private LoginCallback loginCallback;

    public LoginViewModel(LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
        this.loginModel = new UserLoginModel();
    }

    public UserLoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(UserLoginModel loginModel) {
        this.loginModel = loginModel;
    }

    public LoginCallback getLoginCallback() {
        return loginCallback;
    }

    public void setLoginCallback(LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
    }

    public TextWatcher nameTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginModel.setName(s.toString());
            }
        };
    }

    public TextWatcher passwordTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginModel.setPassword(s.toString());
            }
        };
    }

    public void onLoginBtnClicked(View view) {
        if (loginModel.isValid()) {
            loginCallback.onSuccess("Login Successful");
        } else {
            loginCallback.onFailure("Something went wrong");
        }
    }
}
